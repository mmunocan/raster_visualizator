/*
 * Dado un conjunto de imagenes .tiff, obtiene la frecuencia de cada valor contenido.
 * <inputs_file> <input_path>: identificación de los datos de entrada
 */

#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <climits>

using namespace std;
using namespace cimg_library;

int main(int argc, char**argv) {
	if(argc != 3){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path>"<<endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
	string inputs_path = argv[2];
	// Permite leer los archivos .tiff
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	string filename;
	size_t channels, imagenes;
	CImg<float> image;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream index(inputs_file);
	if(!index.is_open()){
		cout << inputs_file << " can not be opened" << endl;
		return -1;
	}
	index >> channels >> imagenes;
	getline(index, filename); // Salto de linea inicial (vacio)
	
	/**************************/
	/* Revisión de cada canal */
	/**************************/
	for(size_t ch = 0; ch < channels; ch++){
		
		int max_value = INT_MIN;
		int min_value = INT_MAX;
		int min, max;
		size_t n_rows, n_cols;
		CImgList<float> images_list;
		
		/**********************************************************/
		/* Revisión cada timestap (para obtene mínimos y máximos) */
		/**********************************************************/
		for(size_t img = 0; img < imagenes; img++){
			
			getline(index, filename);
			string full_filename = inputs_path+filename;
			image.load_tiff(full_filename.c_str());
			min = (int)image.min();
			max = (int)image.max();
			n_rows = (int)image.height();
			n_cols = (int)image.width();
			if(min < min_value)	min_value = min;
			if(max > max_value)  max_value = max;
			images_list.push_back(image);
		}
		
		vector<size_t> histograma((max_value-min_value+1), 0);
		float * data;
		/********************************************/
		/* Revisión cada timestap (para histograma) */
		/********************************************/
		for(size_t img = 0; img < imagenes; img++){
			image = images_list[img];
			data = image.data();
			
			for(size_t cell = 0; cell < n_rows*n_cols; cell++){
				histograma[(int)(data[cell])-min_value]++;
			}
			
		}
		
		/***********************/
		/* Mostrar resultados  */
		/***********************/
		cout << "Canal " << ch << endl;
		cout << "Minimo: " << min_value << endl;
		cout << "Maximo: " << max_value << endl;
		cout << "Histograma:" << endl;
		
		string salida_filename = "channel_"+to_string(ch)+".txt";
		ofstream salida(salida_filename);
		
		size_t cantidad_cells = 0;
		for (size_t value = 0; value < histograma.size(); value++){
			salida << value+min_value << "\t" << histograma[value] << endl;
			cantidad_cells += histograma[value];
		}
		
		salida.close();
		cout << (n_rows*n_cols*imagenes) << "==" << cantidad_cells << endl;
	} // Fin por cada canal
	
	index.close();
	
	return 0;
}