/*
 * Dado un conjunto de imagenes binarias, realiza el filtrado de los valores bajos, comprimiendolos a un solo valor
 * <inputs_file> <input_path>: identificación de los datos de entrada
 * <outputs_path> <filename_base_output>: ubicacion de la salida
 * <n_rasters>: la cantidad de timestaps a procesar
 * <percentaje>: % del rango de valores más bajo a comprimir
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <iomanip> 

using namespace std;

int main(int argc, char ** argv){
	if(argc != 7){
		cout << "Use: " << argv[0] << " <inputs_file> <inputs_path> <outputs_file_base> <outputs_path> <n_raster> <percentaje>" << endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
	string inputs_path = argv[2];
	string outputs_file_base = argv[3];
	string outputs_path = argv[4];
	size_t n_raster = atoi(argv[5]);
	size_t percentaje = atoi(argv[6]);
	
	size_t n_rows, n_cols, k1, k2, n_k1, n_plains;
	string raster_filename;
	int value, min_value = INT_MAX, max_value = INT_MIN;
	
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	outputs_path = outputs_path.at(outputs_path.size()-1) != '/' ? outputs_path.append("/") : outputs_path;
	
	vector<vector<int>> datas(n_raster);
	
	/**********************************/
	/* Lectura del indíce de binarios */
	/**********************************/
	ifstream list_file(inputs_file);
	if(!list_file.is_open()){
		cout << inputs_file << " can not be opened" << endl;
		return -1;
	}
	
	list_file >> n_rows >> n_cols >> k1 >> k2 >> n_k1 >> n_plains;
	
	getline(list_file, raster_filename); // Salto de linea inicial (vacio)
	
	/*************************/
	/* Lectura de la entrada */
	/*************************/
	for(size_t raster = 0; raster < n_raster; raster++){
		getline(list_file, raster_filename); 
		raster_filename = inputs_path+raster_filename;
		ifstream raster_file(raster_filename);
		
		for(size_t cell = 0; cell < n_rows*n_cols; cell++){
			raster_file.read((char *) (& value), sizeof(int));
			datas[raster].push_back(value);
			
			if(value < min_value)	min_value = value;
			if(value > max_value)	max_value = value;
			
		}
		
		raster_file.close();
	}
	
	list_file.close();
	
	/***************************/
	/* Aplicacion del filtrado */
	/***************************/
	unsigned int differences = max_value - min_value;
	int min_allowed = ((differences * percentaje)/100) + min_value;
	size_t affected = 0;
	
	for(size_t raster = 0; raster < n_raster; raster++){
		for(size_t cell = 0; cell < n_rows*n_cols; cell++){
			if(datas[raster][cell] < min_allowed){
				datas[raster][cell] = min_value;
				affected++;
			}
		}
	}
	
	/**************************/
	/* Escritura de la salida */
	/**************************/
	
	string outputs_file = outputs_path+outputs_file_base+"txt";
	
	ofstream list_file_output(outputs_file);
	list_file_output << n_rows << " " << n_cols << " " << k1 << " " << k2 << " " << n_k1 << " " << n_plains << endl;
	
	for(size_t raster = 0; raster < n_raster; raster++){
		raster_filename = outputs_file_base+"IMG"+to_string(raster)+".bin";
		list_file_output << raster_filename << endl;
		raster_filename = outputs_path+raster_filename;
		ofstream raster_file_output(raster_filename);
		
		for(size_t cell = 0; cell < n_rows*n_cols; cell++){
			raster_file_output.write((char *) (& datas[raster][cell]), sizeof(int));
		}
		
		raster_file_output.close();
	}
	
	list_file_output.close();
	
	return 0;
}