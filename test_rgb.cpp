/*
 * Código de prueba para generar imágenes .jpg e imágenes .gif
 */
#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using namespace cimg_library;

int main(int argc, char**argv){
	
	
	int n_rows = 500;
	int n_cols = 500;
	int n_images = 2664;
	int vel_raster = 25;
	string filename_gif = "salida_gif.gif";
	string filename_jpg = "salida_jpg.jpg";
	
	unsigned char * values_plain = new unsigned char[n_rows*n_cols*3];
	CImgDisplay pantalla;
	CImgList<unsigned char> images_list;
	
	for(int img = 0; img < n_images; img++){
	
		for(int i = 0; i < n_rows*n_cols;i++){
			values_plain[i] = 255 - img;
			values_plain[(n_rows*n_cols) + i] = i % 256;
			values_plain[(2*n_rows*n_cols) + i] = img;
		}

		CImg<unsigned char> test_image(values_plain, n_rows, n_cols, 1, 3, false);
		if(img == 0){
			test_image.save_jpeg(filename_jpg.c_str());
		}
		pantalla.display(test_image);
		pantalla.wait(vel_raster);
		images_list.push_back(test_image);
	}
	
	pantalla.close();
	
	// guardado
	images_list.save_gif_external(filename_gif.c_str());
	
	return 0;
}