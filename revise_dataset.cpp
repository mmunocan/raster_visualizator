/*
 * Muestra por pantalla una imágen raster preconcebida como una lista de binarios por cada raster. Además genera un .gif con el raster obtenido
 * <inputs_file> <input_path>: identificación de los datos de entrada
 * <inputs_path>: path en donde se encuentran las imagenes .tiff
 * <n_rasters>: la cantidad de timestaps a procesar
 * <output_filename>: nombre del gif de salida
 * <vel_raster>: la velocidad de la animación
 * <saltos_raster>: cada cuantos rasters los consideramos para hacer el gif (cuando son demasiados rasters a veces no podemos generar el gif, por lo que solo podemos preseleccionar algunos con saltos regulares). Esto no afecta los rasters mostrados por pantalla.
 */
#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <climits>

using namespace std;
using namespace cimg_library;

int main(int argc, char**argv){
	if(argc != 7){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> <output_filename> <vel_raster> <saltos_raster> "<<endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	string output_filename = argv[4];
	size_t vel_raster = atoi(argv[5]);
	size_t saltos_raster = atoi(argv[6]);
	// Permite leer los archivos de entrada sin errores
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	string raster_filename;
	int value;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream input_global(inputs_file);
	if(!input_global.is_open()){
		cout <<  inputs_file << " can not be opened" << endl;
		return -1;
	}
	input_global >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	/******************************/
	/* Mostrar datos por pantalla */
	/******************************/
	float * values_plain = new float[n_rows*n_cols];
	CImgDisplay pantalla;
	CImgList<float> images_list;
	int max_value = INT_MIN;
	int min_value = INT_MAX;
	
	/************************/
	/* Procesar cada raster */
	/************************/
	for(size_t rast = 0; rast < n_elements; rast++){
		
		input_global >> raster_filename;
		ifstream input_raster(inputs_path+raster_filename);
		if(!input_raster.is_open()){
			cout << inputs_path << raster_filename << " can not be opened" << endl;
			return -1;
		}
		
		/*****************************************/
		/* Procesar cada celda dentro del raster */
		/*****************************************/
		for(size_t cell = 0; cell < n_rows * n_cols; cell++){
			input_raster.read((char *) (& value), sizeof(int));
			values_plain[cell] = (float)value;
			
			if(value > max_value) max_value = value;
			if(value < min_value) min_value = value;
		}
		input_raster.close();
		
		/**********************************/
		/* Mostrar por pantalla el raster */
		/**********************************/
		CImg<float> raster(values_plain, n_rows, n_cols, 1, 1, false);
		pantalla.display(raster);
		pantalla.wait(vel_raster);
		
		cout << "Imprimiendo raster " << rast << endl;
		raster.print();
		images_list.push_back(raster);
		
	} // Fin revisión cada raster
	input_global.close();
	pantalla.close();
	
	/**************************/
	/* Generar el archivo gif */
	/**************************/
	
	int diff = max_value - min_value;
	unsigned char pixel = 0;
	CImgList<unsigned char> images_list_rgb;
	unsigned char * values_rgb = new unsigned char[n_rows*n_cols*3];
	
	/**********************************/
	/* Procesar los rasters del salto */
	/**********************************/
	for(size_t rast = 0; rast < n_elements; rast+=saltos_raster){
		CImg<float> raster = images_list.at(rast);
		values_plain = raster.data();
		
		for(size_t cell = 0; cell < n_rows * n_cols; cell++){
			
			value = (int)values_plain[cell] - min_value;
			pixel = (value * 255 / diff);
			
			values_rgb[cell] = pixel;//255;
			values_rgb[(n_rows*n_cols) + cell] = pixel;
			values_rgb[(2*n_rows*n_cols) + cell] = pixel;//0;
		}
		
		CImg<unsigned char> raster_rgb(values_rgb, n_rows, n_cols, 1, 3, true);
		images_list_rgb.push_back(raster_rgb);
		
	}
	
	images_list_rgb.save_gif_external(output_filename.c_str(), vel_raster * saltos_raster);
	
	return 0;
}