/*
 * Convierte una imagen con varios timestap y canales en un grupo de datasets binarios.
 * Por cada canal genera un dataset que puede procesar posteriormente el k2raster
 * <inputs_file> <inputs_path>: identificacion de la entrada
 * <outputs_path> <filename_base_output>: ubicacion de la salida
 * <decimals_considerers>: Cantidad de decimales a considerar a la hora de generar el archivo.
 * Las celdas de entrada son float, mientras que la salida debe ser int
 */

#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;
using namespace cimg_library;

int main(int argc, char**argv) {
	if(argc != 6){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <outputs_path> <filename_base_output> <decimals_considerers>"<< endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
	string inputs_path = argv[2];
	string outputs_path = argv[3];
	string filename_base_output = argv[4];
	int decimals_considerers = atoi(argv[5]);
	// Permite leer los archivos .tiff
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	outputs_path = outputs_path.at(outputs_path.size()-1) != '/' ? outputs_path.append("/") : outputs_path;
	size_t channels, imagenes, n_rows, n_cols;
	string filename;
	int value;
	CImg<float> image;
	float * data_images;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream index(inputs_file);
	if(!index.is_open()){
		cout << inputs_file << " can not be opened" << endl;
		return -1;
	}
	index >> channels >> imagenes;
	getline(index, filename); // Salto de linea inicial (vacio)
	
	/**************************/
	/* Revisión de cada canal */
	/**************************/
	for(size_t ch = 0; ch < channels; ch++){
		string filename_index_channel = outputs_path+filename_base_output+"CH"+to_string(ch)+".txt";
		ofstream index_channel(filename_index_channel);
		if(!index_channel.is_open()){
			cout << filename_index_channel << " can not be opened" << endl;
			return -1;
		}
		
		/*****************************************/
		/* Revisión y procesado de cada timestap */
		/*****************************************/
		for(size_t img = 0; img < imagenes; img++){
			string filename_image;
			getline(index, filename_image);
			string full_filename = inputs_path+filename_image;
			image.load_tiff(full_filename.c_str());
			data_images = image.data();
			
			/*********************************/
			/* Actualizacion del inputs_file */
			/*********************************/
			if(img == 0){
				n_rows = image.width();
				n_cols = image.height();
				index_channel << n_rows << " " << n_cols << " 4 2 4 2" << endl;
			}
			
			string filename_raster = filename_base_output+"CH"+to_string(ch)+"IMG"+to_string(img)+".bin";
			index_channel << filename_raster;
			filename_raster = outputs_path + filename_raster;
			if(img != imagenes-1){
				index_channel << endl;
			}
			
			/*********************************/
			/* Generacion del raster binario */
			/*********************************/
			ofstream raster(filename_raster);
			if(!raster.is_open()){
				cout << filename_raster << " can not be opened" << endl;
				return -1;
			}
			
			for(size_t cell = 0; cell < n_rows * n_cols; cell++){
				value = (int)(data_images[cell] * pow(10, decimals_considerers));
				raster.write((char *) (& value), sizeof(int));
				
			}
			
			raster.close();
		}
		index_channel.close();
	}
	
	return 0;
}