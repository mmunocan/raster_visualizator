/*
 * Dado un conjunto imagenes tiff, realiza el filtrado de los valores bajos, comprimiendolos a un solo valor
 * <inputs_file> <input_path>: identificación de los datos de entrada
 * <percentaje>: % del rango de valores más bajo a comprimir
 * <outputs_path> <filename_base_output>: ubicacion de la salida
 */

#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <iomanip> 

using namespace std;
using namespace cimg_library;

int main(int argc, char**argv) {
	if(argc != 6){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <percentaje> <outputs_path> <filename_base_output>"<<endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
	string inputs_path = argv[2];
	int percentaje = atoi(argv[3]);
	string outputs_path = argv[4];
	string filename_base_output = argv[5];
	// Permite leer los archivos .tiff
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	outputs_path = outputs_path.at(outputs_path.size()-1) != '/' ? outputs_path.append("/") : outputs_path;
	string filename;
	size_t channels, imagenes;
	CImg<float> image;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream index(inputs_file);
	if(!index.is_open()){
		cout << inputs_file << " can not be opened" << endl;
		return -1;
	}
	index >> channels >> imagenes;
	getline(index, filename); // Salto de linea inicial (vacio)
	
	/**************************/
	/* Revisión de cada canal */
	/**************************/
	for(size_t ch = 0; ch < channels; ch++){
		
		int max_value = INT_MIN;
		int min_value = INT_MAX;
		int min, max, value;
		size_t n_rows, n_cols;
		string filename_raster;
		CImgList<float> images_list;
		
		
		/**************************************************/
		/* Preparacion del index para el raster del canal */
		/**************************************************/
		string filename_index_channel = outputs_path+filename_base_output+"_"+to_string(ch)+".txt";
		
		ofstream index_channel(filename_index_channel);
		if(!index_channel.is_open()){
			cout << filename_index_channel << " can not be opened" << endl;
			return -1;
		}
		
		/*************************************************************/
		/* Revisión cada timestap (para tener el minimo y el maximo) */
		/*************************************************************/
		for(size_t img = 0; img < imagenes; img++){
			
			getline(index, filename);
			string full_filename = inputs_path+filename;
			image.load_tiff(full_filename.c_str());
			
			if(img == 0){
				n_rows = image.width();
				n_cols = image.height();
			}
			
			min = image.min();
			max = image.max();
			if(min < min_value)	min_value = min;
			if(max > max_value)  max_value = max;
			
			images_list.push_back(image);
			
		}
		
		index_channel << n_rows << " " << n_cols << " 4 2 4 2" << endl;
		
		/*******************************/
		/* Obtención del nuevo dataset */
		/*******************************/
		int differences = max_value - min_value;
		int min_allowed = ((differences * percentaje)/100) + min_value;
		size_t afectadas = 0;
		
		for(size_t img = 0; img < imagenes; img++){
			image = images_list[img];
			float * image_data = image.data();
			float * new_image_data = new float[n_rows*n_cols];
			
			/*********************************/
			/* Actualizacion del inputs_file */
			/*********************************/
			filename_raster = filename_base_output+"CH"+to_string(ch)+"IMG"+to_string(img)+".bin";
			index_channel << filename_raster;
			filename_raster = outputs_path + filename_raster;
			if(img != imagenes-1){
				index_channel << endl;
			}
			
			/************************************/
			/* Obtención de los datos de salida */
			/************************************/
			for(size_t cell = 0; cell < n_rows*n_cols; cell++){
				if(image_data[cell] < min_allowed){
					new_image_data[cell] = min_value;
					afectadas++;
				}else{
					new_image_data[cell] = image_data[cell];
				}
			}
			
			
			/************************************/
			/* Escritura de los datos de salida */
			/************************************/
			
			ofstream raster(filename_raster);
			if(!raster.is_open()){
				cout << filename_raster << " can not be opened" << endl;
				return -1;
			}
			
			for(size_t cell = 0; cell < n_rows * n_cols; cell++){
				value = (int)(new_image_data[cell]);
				raster.write((char *) (& value), sizeof(int));
				
			}
			raster.close();
			
		}
		
		float per_afectadas = (afectadas *100) / (imagenes*n_rows*n_cols);
		cout << fixed << setprecision(2) << per_afectadas << "% de las celdas afectadas" << endl;
		
		index_channel.close();
		
	} // Fin por cada canal
	
	
	index.close();
	
	return 0;
}