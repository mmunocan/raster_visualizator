CPP=g++

BINS=ejemplo image_to_binary image_zorder revise_dataset revise_list_images test_rgb reduccion_rango get_histograma reduccion_binario
		
CPPFLAGS=-lX11 -L/usr/X11R6/lib -lm -lpthread
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin:   $(BINS)

ejemplo:
	$(CPP) -o $(DEST)/ejemplo ejemplo.cpp  $(CPPFLAGS) 
	
image_to_binary:
	 $(CPP) -o $(DEST)/image_to_binary image_to_binary.cpp  $(CPPFLAGS) 
	
image_zorder:
	 $(CPP) -o $(DEST)/image_zorder image_zorder.cpp  $(CPPFLAGS) 
	
revise_dataset:
	 $(CPP) -o $(DEST)/revise_dataset revise_dataset.cpp  $(CPPFLAGS) 
	
revise_list_images:
	 $(CPP) -o $(DEST)/revise_list_images revise_list_images.cpp  $(CPPFLAGS) 
	
test_rgb:
	 $(CPP) -o $(DEST)/test_rgb test_rgb.cpp  $(CPPFLAGS) 

reduccion_rango:
	 $(CPP) -o $(DEST)/reduccion_rango reduccion_rango.cpp  $(CPPFLAGS) 

get_histograma:
	 $(CPP) -o $(DEST)/get_histograma get_histograma.cpp  $(CPPFLAGS) 
	
reduccion_binario:
	$(CPP) -o $(DEST)/reduccion_binario reduccion_binario.cpp  $(CPPFLAGS) 

clean:
	rm -f   $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
