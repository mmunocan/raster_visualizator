/*
 * Convierte un  raster temporal (3D) a una imagen ancha en 2D usando zorder
 * La muestra por pantalla y la guarda en un archivo .bmp
 * <inputs_file> <input_path>: identificación de los datos de entrada
 * <imputs_path>: path en donde se encuentran las imagenes .tiff
 * <n_rasters>: la cantidad de timestaps a procesar
 * <output_filename>: nombre de la imagen de salida
 */

#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <cmath>

using namespace std;
using namespace cimg_library;

/****************************************************/
/* Funciones a cargo de obtener el código de Morton */
/****************************************************/
unsigned long interleave_uint32_with_zeros(unsigned int input)  {
    unsigned long word = input;
    word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
    word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
    word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
    word = (word ^ (word << 2 )) & 0x3333333333333333;
    word = (word ^ (word << 1 )) & 0x5555555555555555;
    return word;
}

unsigned long get_zorder(unsigned int p, unsigned int q){
	return interleave_uint32_with_zeros(q) | (interleave_uint32_with_zeros(p) << 1);
}

unsigned int get_lado(unsigned int filas, unsigned int columnas){
	if(filas >= columnas){
		return  (1 << 32-__builtin_clz(filas-1));
	}else{
		return  (1 << 32-__builtin_clz(columnas-1));
	}
}



int main(int argc, char**argv){
	if(argc != 5){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> <output_filename> "<<endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	string output_filename = argv[4];
	// Permite leer los archivos de entrada sin errores
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	string raster_filename;
	int value;
	int max_value = INT_MIN;
	int min_value = INT_MAX;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream input_global(inputs_file);
	if(!input_global.is_open()){
		cout <<  inputs_file << " can not be opened" << endl;
		return -1;
	}
	input_global >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	/*******************************/
	/* Creacion de la imagen larga */
	/*******************************/
	size_t lado = get_lado(n_rows, n_cols);
	size_t double_lado = lado * lado;
	size_t all_cells = double_lado * n_elements;
	float * values_plain = new float[all_cells];
	for(size_t cell = 0; cell < all_cells; cell++) values_plain[cell] = 0;
	
	/************************/
	/* Procesar cada raster */
	/************************/
	for(size_t rast = 0; rast < n_elements; rast++){
		
		input_global >> raster_filename;
		ifstream input_raster(inputs_path+raster_filename);
		if(!input_raster.is_open()){
			cout << inputs_path << raster_filename << " can not be opened" << endl;
			return -1;
		}
		
		/*****************************************/
		/* Procesar cada celda dentro del raster */
		/*****************************************/
		for(size_t row = 0; row < n_rows; row++){
			for(size_t col = 0; col < n_cols; col++){
				
				input_raster.read((char *) (& value), sizeof(int));
				values_plain[(double_lado * rast) + get_zorder(row, col)] = (float)value;
				
				if(value > max_value) max_value = value;
				if(value < min_value) min_value = value;
			}
		}
		input_raster.close();
		
	}
	input_global.close();
	
	/**************************************/
	/* Creación de la imagen 2D en Zorder */
	/**************************************/
	CImg<float> zorder_image(values_plain, double_lado, n_elements, 1, 1, true);
	zorder_image.display();
	
	/******************************/
	/* Guardar la imagen generada */
	/******************************/
	unsigned char * values_rgb = new unsigned char[all_cells*3];
	int diff = max_value - min_value;
	unsigned char pixel = 0;
	
	for(size_t cell = 0; cell < all_cells; cell++){
		value = (int)values_plain[cell] - min_value;
		pixel = (value * 255 / diff);

		values_rgb[cell] = 255;
		values_rgb[all_cells + cell] = pixel;
		values_rgb[(2*all_cells) + cell] = 0;
		
	}
	
	CImg<unsigned char> zorder_image_rgb(values_rgb, double_lado, n_elements, 1, 3, true);
	zorder_image_rgb.save_bmp(output_filename.c_str());
	
	return 0;
}