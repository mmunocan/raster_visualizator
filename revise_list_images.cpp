/*
 * Muestra por pantalla una imágen de varios timestaps con varios canales, y guarda un gif por cada canal.
 * <inputs_file>: descriptor de la imágen. Su primera linea contiene 2 valores: cantidad de canales y cantidad de rasters
 * Desde la línea 2 a la <canales>x<timestap>: el nombre de cada imagen  .tiff
 * <inputs_path>: path en donde se encuentran las imagenes .tiff
 * <vel_raster>: la velocidad de la animación
 * <show_info>: mostrar la información por raster
 * <saltos_raster>: cada cuantos rasters los consideramos para hacer el gif (cuando son demasiados rasters a veces no podemos generar el gif, por lo que solo podemos preseleccionar algunos con saltos regulares). Esto no afecta los rasters mostrados por pantalla.
 */

#include "CImg.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <climits>

using namespace std;
using namespace cimg_library;

int main(int argc, char**argv) {
	if(argc != 6){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <vel_raster> <show_info> <saltos_raster>"<<endl;
		return -1;
	}
	
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
	string inputs_path = argv[2];
	int vel_raster = atoi(argv[3]);
	int show_info = atoi(argv[4]);
	int saltos_raster = atoi(argv[5]);
	// Permite leer los archivos .tiff
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	string filename;
	size_t channels, imagenes;
	CImg<float> image;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream index(inputs_file);
	if(!index.is_open()){
		cout << inputs_file << " can not be opened" << endl;
		return -1;
	}
	index >> channels >> imagenes;
	getline(index, filename); // Salto de linea inicial (vacio)
	
	/**************************/
	/* Revisión de cada canal */
	/**************************/
	CImgDisplay pantalla;
	for(size_t ch = 0; ch < channels; ch++){
		
		int max_value = INT_MIN;
		int min_value = INT_MAX;
		int min, max;
		size_t n_rows, n_cols;
		CImgList<float> images_list;
		
		/***************************************/
		/* Revisión y muestra de cada timestap */
		/***************************************/
		for(size_t img = 0; img < imagenes; img++){
			
			getline(index, filename);
			string full_filename = inputs_path+filename;
			image.load_tiff(full_filename.c_str());
			
			pantalla.display(image);
			pantalla.wait(vel_raster);
			
			
			min = (int)image.min();
			max = (int)image.max();
			n_rows = (int)image.height();
			n_cols = (int)image.width();
			if(min < min_value)	min_value = min;
			if(max > max_value)  max_value = max;
			
			if(show_info){
				cout << "Channel: " << ch << ", image: " << img << endl;
				image.print();
			}
			
			images_list.push_back(image);
			
			
		}
		
		pantalla.wait(vel_raster*100);
		
		int diff = max_value - min_value;
		unsigned char pixel = 0;
		int value = 0;
		
		/*******************************************/
		/* Generación de cada timestap para el gif */
		/*******************************************/
		CImgList<unsigned char> images_list_rgb;
		unsigned char * values_rgb = new unsigned char[n_rows*n_cols*3];
		for(size_t img = 0; img < imagenes; img+=saltos_raster){
			image = images_list.at(img);
			float * values_plain = image.data();	
			
			for(size_t cell = 0; cell < n_rows * n_cols; cell++){

				value = (int)values_plain[cell] - min_value;
				pixel = (value*255) / diff;

				values_rgb[cell] = pixel;
				values_rgb[(n_rows*n_cols) + cell] = pixel;
				values_rgb[(2*n_rows*n_cols) + cell] = pixel;
			}

			CImg<unsigned char> raster_rgb(values_rgb, n_rows, n_cols, 1, 3, true);
			images_list_rgb.push_back(raster_rgb);
			
		}
		
		/*************************************************/
		/* Guardar el gif. El nombre es "channel_ch.gif" */
		/*************************************************/
		string filename = "channel_"+to_string(ch)+".gif";
		images_list_rgb.save_gif_external(filename.c_str(), vel_raster * saltos_raster);
		
		pantalla.wait(vel_raster*100);
		
	} // Fin por cada canal
	
	pantalla.close();
	index.close();
    return 0;
}
